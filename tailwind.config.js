/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./public/index.html",
        "./src/**/*.{vue,js,jsx}"
    ],
    theme: {
        extend: {},
    },
    plugins: [],
}

