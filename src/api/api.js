
import axios from 'axios';

const api = axios.create({
    baseURL: 'https://booking-com15.p.rapidapi.com/api/v1/', // Replace with your Rapid API base URL
    headers: {
        'X-RapidAPI-Key': '92fdb315a3mshd6ddeac570c368ep157699jsnd93ed73c0f13', // Replace with your Rapid API key
        'X-RapidAPI-Host': 'booking-com15.p.rapidapi.com'
    }
});


export default api;
