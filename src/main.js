import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import { OhVueIcon, addIcons } from "oh-vue-icons";
import { BiBellFill, MdLockRound, RiShieldUserFill, BiHandThumbsUp, CoLifeRing, GiCommercialAirplane, HiSolidLocationMarker, CoLocationPin, IoSearchOutline, BiEyeSlash, BiEye, MdBedroomparentOutlined, BiExclamationTriangle, IoCalendarOutline, OiLocation, MdKeyboardarrowdown, RiUserLine } from "oh-vue-icons/icons";



import VueTelInput from 'vue-tel-input';
import 'vue-tel-input/vue-tel-input.css';



addIcons({
    name: "plane-tilted",
    width: 25,
    height: 25,
    d: 'M3.414 13.778 2 15.192l4.949 2.121 2.122 4.95 1.414-1.414-.707-3.536L13.091 14l3.61 7.704 1.339-1.339-1.19-10.123 2.828-2.829a2 2 0 1 0-2.828-2.828l-2.903 2.903L3.824 6.297 2.559 7.563l7.644 3.67-3.253 3.253-3.536-.708z'
}, BiBellFill, GiCommercialAirplane, HiSolidLocationMarker, CoLocationPin, IoSearchOutline, BiEyeSlash, BiEye, MdBedroomparentOutlined,
    BiExclamationTriangle, MdLockRound, IoCalendarOutline, OiLocation, MdKeyboardarrowdown, RiUserLine, CoLifeRing, BiHandThumbsUp, RiShieldUserFill);

createApp(App).component("v-icon", OhVueIcon).use(VueTelInput).use(router).mount("#app");
