import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";


const routes = [
    {
        path: "/",
        name: "home",
        component: HomeView,

    },
    {
        path: "/sign-in",
        name: "SignIn",
        meta: {
            hideNav: true,
            hideFooter: true,
        },
        component: () =>
            import("../views/SignView.vue"),
    },
    {
        path: "/register",
        name: "Register",
        meta: {
            hideNav: true,
            hideFooter: true,
        },
        component: () =>
            import("../views/RegisterView.vue"),
    },
    {
        path: "/search",
        name: "Search",
        component: () =>
            import("../views/SearchView.vue"),
        meta: {
            dark: true,
        }
    },
    {
        path: "/details/:id",
        name: "Details",
        component: () =>
            import("../views/ProductDetailView.vue"),
        meta: {
            fullWidth: true,
            bgColor: "#F4F4F4"
        }
    },
    {
        path: "/my-trips",
        name: "MyTrips",
        component: () =>
            import("../views/TripsView.vue"),
        meta: {
            bgColor: "#F4F4F4"
        }
    },
    {
        path: "/checkout",
        name: "Checkout",
        component: () =>
            import("../views/CheckoutView.vue"),
        meta: {
            bgColor: "#F4F4F4"
        }
    }
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
});

router.beforeEach((to) => {
    if (to.name == 'home') return true
    if (isLoggedIn()) {
        if (to.name == 'SignIn' || to.name == 'Register') return '/'
        else return true
    } else {
        if (to.name == 'SignIn' || to.name == 'Register') return true
        else return '/sign-in'
    }

})

function isLoggedIn() {
    return !!localStorage.getItem("token");
}

export default router;
